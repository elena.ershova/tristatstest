import { waitCondition } from '../../lib/Waiter';
import { app } from '../pages/index';
import { BasePage } from './BasePage';

class MainPage extends BasePage {
    search = "[href = '/rus/search']";
    searchEventInput = "[ng-model = 'races.searchQuery']";
    eventRow = "//tr[contains(@ng-repeat, 'races.data')]";
    eventItemLink = "a[href *='/rus/result/']";

    constructor(page) {
        super(page);
        this.check();
    }

    check() {
        waitCondition(async () => {
            return this.page.isVisible(this.search);
        }, 'Waiting user search link in Main page');
        waitCondition(async () => {
            return this.page.isVisible(this.searchEventInput);
        }, 'Waiting search event field in Main page');
        waitCondition(async () => {
            this.page.isVisible(this.eventRow);
        }, 'Waiting event row in Main page');
        this.logger.info('Main page is loaded');
    }

    async clickSearchUser() {
        this.page.click(this.search);
        return app.SearchPage(this.page);
    }

    async searchEvent(query) {
        await this.page.fill(this.searchEventInput, query);
        return app.MainPage(this.page);
    }

    async isEventPresent(name) {
        let locator = `${this.eventRow}//*[text()='${name}']`;
        let text = await this.page.textContent(locator);
        if (text === name) {
            return true;
        }
        return false;
    }

    async openEvent(name) {
        let locator = `${this.eventRow}//*[text()='${name}']`;
        await this.page.click(locator);
        return app.EventPage(this.page);
    }
}

export { MainPage };
