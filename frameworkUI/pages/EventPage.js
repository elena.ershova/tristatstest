import { app } from '../pages/index';
import { waitCondition } from '../../lib/Waiter';
import { BasePage } from './BasePage';

class EventPage extends BasePage {
    mainBlock = '#content';
    header = 'h1.ng-binding';
    results = '#results';
    resultRow = 'tr[ng-repeat-start]';
    resultUser = "a[href *= '/profile']";
    female = '.fa-venus';
    male = '.fa-mars';
    toggleFemale = "[ng-class *= 'isShowWoman']";
    toggleMale = "[ng-class *= 'isShowMan']";
    toggleMaleDeselected = "[ng-class *= 'isShowMan'].btn-default"
    attrDefault = 'btn-default';

    constructor(page) {
        super(page);
        this.check();
    }

    check() {
        waitCondition(async () => {
            return await this.page.isVisible(this.mainBlock)
        }, 'Main block visibility in event page');
        waitCondition(async () => {
            return await this.page.isVisible(this.header)
        }, 'Header visibility in event page');
        waitCondition(async () => {
            return await this.page.isVisible(this.resultUser);
        }, 'Waiting all participants', 1000, 300);
        this.logger.info('Event page is loaded');
    }

    async getHeader() {
        let headerText = await this.page.innerText(this.header, 1000);
        return headerText.trim();
    }

    async getParticipantsCount() {
        await waitCondition(async () => {
            return (await this.page.$$(this.resultUser)).length > 0
        }, 'Waiting all participants', 1000, 500);
        let participants = await this.page.$$(this.resultUser);
        return participants.length;
    }

    async openUserPage(name) {
        this.logger.info(`Opening user profile for ${name}`)
        await waitCondition(async () => {
            return (await this.page.$$(this.resultUser)).length > 0
        }, 'Waiting all participants', 1000, 500);
        let users = await this.page.$$(this.resultUser);
        for (let user of users) {
            let linkText = await user.innerText();
            if (linkText === name) {
                await user.click();
                return app.UserPage(this.page);
            }
        }
        throw new Error(`User not found by name ${name}`);
    }

    async deselectMale() {
        const initialResult = await this.page.textContent(this.resultUser);
        await this.page.click(this.toggleMale);
        waitCondition(async () => {
            let afterClickResult = await this.page.textContent(this.resultUser);
            return afterClickResult !== initialResult;
        }, 'Deselect male and wait for new results');
        return app.EventPage(this.page);
    }

    async isMaleResultsPresent() {
        return await this.page.isVisible(this.male);
    }

    async isFemaleResultsPresent() {
        return await this.page.isVisible(this.female);
    }

}

export { EventPage };
