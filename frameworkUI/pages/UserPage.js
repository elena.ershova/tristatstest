import { waitCondition } from '../../lib/Waiter';
import { endpoints } from '../../frameworkAPI/conf/endpoints';
import { BasePage } from './BasePage';

class UserPage extends BasePage {
    mainBlock = '#content';
    userFlag = '.ng-binding .flag';
    header = 'h1.ng-binding';
    resultRow = 'tr[ng-repeat]';
    resultLink = "a[href *= '/result/']"

    constructor(page) {
        super(page);
        // this.check();
    }

    check() {
        waitCondition(() => {
            return this.page.url().contains(endpoints.profile);
        }, 'Waiting url for profile page');
        waitCondition(async () => {
            return await this.page.isVisible(this.mainBlock);
        }, 'Main block visibility in user page');
        waitCondition(async () => {
            return await this.page.isVisible(this.header);
        }, 'Header visibility in user page');
        waitCondition(async () => {
            return await this.page.isVisible(this.header);
        }, 'Flag visibility in user page');
        waitCondition(async () => {
            return await this.page.isVisible(this.resultRow);
        }, 'Waiting at least one result', 800, 400);
        this.logger.info('User page is loaded');
    }

    async getUserName() {
        this.logger.debug('Getting user name')
        await waitCondition(async () => {
            return (await this.page.textContent(this.header)).trim().length > 0
        }, 'Waiting user name', 2000, 500);
        let text = await this.page.textContent(this.header);
        return text.trim();
    }

    async isResultPresent(name) {
        let results = await this.page.$$(this.resultLink);
        for (let res of results) {
            let text = await res.textContent();
            if (text === name) {
                return true;
            }
        }
        return false;
    }
}

export { UserPage };
