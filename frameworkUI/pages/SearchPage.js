import { waitCondition } from "../../lib/Waiter";
import { BasePage } from "./BasePage";

class SearchPage extends BasePage {
    mainBlock = '#content';
    searchInput = "input[ng-model='searchQuery']";
    searchButton = '.input-group-addon';
    searchResults = '#results';
    resultRow = "[ng-repeat *= 'results.data']"

    constructor(page) {
        super(page);
        this.check();
    }

    check() {
        waitCondition(async () => {
            return await this.page.isVisible(this.mainBlock);
        }, 'Waiting main block');
        waitCondition(async () => {
            return await this.page.isVisible(this.searchInput);
        }, 'Waiting search input');
        waitCondition(async () => {
            return await this.page.isVisible(this.searchButton);
        }, 'Waiting search button');
        this.logger.info('Search page is loaded');
    }

    async search(query) {
        await this.page.fill(this.searchInput, query);
        await this.page.click(this.searchButton);
        await this.page.waitForSelector(this.searchResults);
    }

    async getSearchResults() {
        let rows = await this.page.$$(this.resultRow);
        let rowsTxt = rows.map(async row => {
            return await row.textContent();
        });
        return rowsTxt;
    }
}

export { SearchPage };
