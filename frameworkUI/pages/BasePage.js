import log4js from 'log4js';

class BasePage {

    constructor(page) {
        this.page = page;
        this.logger = log4js.getLogger();
        this.logger.level = 'debug';
    }
}

export { BasePage };
