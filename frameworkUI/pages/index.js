import { MainPage } from './MainPage';
import { SearchPage } from './SearchPage';
import { EventPage } from './EventPage';
import { UserPage } from './UserPage';

const app = {
    MainPage: (page) => new MainPage(page),
    SearchPage: (page) => new SearchPage(page),
    EventPage: (page) => new EventPage(page),
    UserPage: (page) => new UserPage(page),
}

export { app };
