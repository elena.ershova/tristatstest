const endpoints = {
    baseUrl: 'http://tristats.ru/api',
    race: '/rus/race',
    raceResults: '/result',
    profile: '/rus/profile',
    profileBest: '/rus/best',
    profileResults: '/rus/result',
}

export { endpoints };
