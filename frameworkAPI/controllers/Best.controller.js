import { endpoints } from '../conf/endpoints';
import { Base } from './Base.controller';

class Best extends Base {
    constructor(name) {
        super(`${endpoints.profileBest}/${name}`);
        this.name = name;
    }
}

export { Best };
