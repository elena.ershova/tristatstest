import { Base } from './Base.controller';
import { endpoints } from '../conf/endpoints';

class Race extends Base {
    constructor() {
        super(endpoints.race);
    }
}

export { Race };
