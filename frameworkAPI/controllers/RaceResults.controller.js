import { Base } from './Base.controller';
import { endpoints } from '../conf/endpoints';

class RaceResults extends Base {
    constructor(event) {
        super(`${endpoints.raceResults}${event}`);
        this.event = event;
    }
}

export { RaceResults };
