import fetch from 'node-fetch';
import { endpoints } from '../conf/endpoints';
import log4js from 'log4js';

class Base {
    constructor(url) {
        this.url = `${endpoints.baseUrl}${url}`;
        this.logger = log4js.getLogger();
        this.logger.level = 'debug';
    }

    async get() {
        this.logger.info(`API URL: ${this.url}`);
        let response = await fetch(this.url);
        return response;
    }
}

export { Base };
