import { endpoints } from '../conf/endpoints';
import { Base } from './Base.controller';

class Profile extends Base {
    constructor(name) {
        super(`${endpoints.profile}/${name}`);
        this.name = name;
    }
}

export { Profile };
