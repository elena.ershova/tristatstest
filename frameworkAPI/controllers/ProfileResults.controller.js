import { Base } from './Base.controller';
import { endpoints } from '../conf/endpoints';

class ProfileResults extends Base {
    constructor(name) {
        super(`${endpoints.profileResults}/${name}`);
        this.name = name;
    }
}

export { ProfileResults };
