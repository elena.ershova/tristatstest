import { Race } from './Race.controller';
import { RaceResults } from './RaceResults.controller';
import { Profile } from './Profile.controller';
import { Best } from './Best.controller';
import { ProfileResults } from './ProfileResults.controller';

const api = {
    Race: () => new Race(),
    RaceResults: (event) => new RaceResults(event),
    Profile: (name) => new Profile(name),
    Best: (name) => new Best(name),
    ProfileResults: (name) => new ProfileResults(name),
};

export { api };
