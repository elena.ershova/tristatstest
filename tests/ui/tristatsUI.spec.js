import chai, { use } from 'chai';
import { runChrome, stopChrome, gotoUrl } from '../../lib/Browser'
import { urls } from '../../frameworkUI/const/urls';
import { app } from '../../frameworkUI/pages';
import { Navigation } from '../../lib/Navigation';

const { expect } = chai;
const NAME = 'Ершова Елена';
const EVENT_QUERY = 'Zavidovo';
const EVENT_FULLNAME = 'IronStar Zavidovo Olympic 2021';
const USER_NAME = 'Permitin Vasiliy';
const USER_NAME_RUS = 'Пермитин Василий';
const EVENT_URL = '/ironstar/zavidovo/olympic/2021';

describe('Tristats.ru UI tests', () => {
    let page;

    beforeEach(async () => {
        await runChrome();
        page = await gotoUrl(urls.baseUrl);
    })

    afterEach(async () => {
        await stopChrome();
    })

    it('Test search user', async () => {
        const searchPage = await (app.MainPage(page)).clickSearchUser();
        await searchPage.search(NAME);
        const results = await searchPage.getSearchResults();
        const firstResult = await results[0];
        expect(firstResult, 'First found user is wrong').to.contains(NAME);
    })
    it('Test search event', async () => {
        let searchRes = await app.MainPage(page).searchEvent(EVENT_QUERY);
        expect(await searchRes.isEventPresent(EVENT_FULLNAME), `Event not found: ${EVENT_FULLNAME}`).to.be.true;
    })
    it('Test open event', async () => {
        let mainPageSearch = await app.MainPage(page).searchEvent(EVENT_QUERY);
        expect(await mainPageSearch.isEventPresent(EVENT_FULLNAME)).to.be.true;
        let eventPage = await mainPageSearch.openEvent(EVENT_FULLNAME);
        let participants = await eventPage.getParticipantsCount();
        expect(participants, `Results are not available, participants count = ${participants}`).to.be.greaterThan(0);
        let header = await eventPage.getHeader();
        expect(header, 'Wrong header').to.equal(EVENT_FULLNAME);
    })
    it('Test open user from result', async () => {
        let eventPage = await (new Navigation(page)).gotoEvent(EVENT_URL);
        let userPage = await eventPage.openUserPage(USER_NAME);
        expect(await userPage.getUserName(), 'Wrong user name').to.equal(USER_NAME_RUS);
        expect(await userPage.isResultPresent(EVENT_FULLNAME), `Not found user result: ${EVENT_FULLNAME}`).to.be.true;
    })
    it('Test switch gender group results', async () => {
        let eventPage = await (new Navigation(page)).gotoEvent(EVENT_URL);
        expect(await eventPage.isMaleResultsPresent(), 'Men should be present').to.be.true;
        await eventPage.deselectMale();
        expect(await eventPage.isFemaleResultsPresent(), 'Results should contain Female').to.be.true;
        expect(await eventPage.isMaleResultsPresent(), 'Results should not contain Men').to.be.false;
    })
})
