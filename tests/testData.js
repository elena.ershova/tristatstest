const data = {
    raceResults: '/ironstar/zavidovo/olympic/2021',
    profileName: 'ershova-elena',
    profileNameRus: 'Ершова Елена',
    profileSurnameEng: 'Ershova',
    testUserResult: {
        ResultId: 6263172,
        ProfileUrl: '/rus/profile/permitin-vasiliy',
        DivisionUrl: '/m30-34',
        RacerUrl: '/permitin-vasiliy',
        Racer: 'Permitin Vasiliy',
        Country: 'RUS',
        Division: 'M30-34',
        Sex: 'M',
        DivisionRank: 1,
        Names: ['Плавание', 'Т1', 'Вел', 'Т2', 'Бег', 'Финиш'],
        Times: ['14:17', '1:09', '1:05:02', '1:18', '33:03', '1:54:49'],
        SexRanks: [1, 4, 1, 16, 1, 1]
    },
}

export { data };
