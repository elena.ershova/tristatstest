import chai from 'chai';
import { api } from '../../frameworkAPI/controllers';
import { data } from '../testData';

const { expect } = chai;

describe('Tristats.ru API tests', () => {
    it('Test get races list', async () => {
        const response = await api.Race().get();
        expect(response.status).to.equal(200);
        let races = [];
        races = await response.json();
        expect(await races.length).to.greaterThan(0, 'Races count = 0');

        const race = await races[0];
        expect(race.CountryCount, 'Country count is null').not.to.be.null;
        expect(race.UiCountry, 'UI Country is null').not.to.be.null;
        expect(Number.parseInt(race.Year), 'Year should be > 2021').to.greaterThanOrEqual(2021);
        expect(race.EventUrl, 'Event url is null').not.to.be.null;
        expect(race.RaceUrl, 'Race url is null').not.to.be.null;
        expect(race.RaceName, 'Race name is null').not.to.be.null;
        expect(race.Date, 'Date is null').not.to.be.null;
        expect(race.Country, 'Country is null').not.to.be.null;
        expect(Number.parseInt(race.FemaleCount), 'Females count < 0').to.greaterThanOrEqual(0);
        expect(Number.parseInt(race.RacerCount), 'Racers count = 0').to.greaterThan(0);
    })
    it('Test get race results', async () => {
        const response = await api.RaceResults(data.raceResults).get();
        expect(response.status).to.equal(200);
        let results = await response.json();
        expect(results.length, 'Race results should be > 1').to.greaterThan(1);
        const result = await results[0];
        expect(result.ResultId, `Result id should be ${data.testUserResult.ResultId}`).to.equal(data.testUserResult.ResultId);
    })
    it('Test get user', async () => {
        const response = await api.Profile(data.profileName).get();
        expect(response.status).to.equal(200);
        let profileInfo = await response.json();
        //expect(profileInfo.RatingPointsCurrent, 'Rating points').to.greaterThan(0);
        //expect(profileInfo.RatingPositionCurrent, 'Rating position').to.greaterThan(0);
        expect(profileInfo.RatingPointsPrevious, 'Rating point prev').to.greaterThan(0);
        expect(profileInfo.RatingPointsBeforePrevious, 'Rating points before prev').to.greaterThan(0);
        expect(profileInfo.RatingPositionBeforePrevious, 'Rating position before prev').to.greaterThan(0);
        expect(profileInfo.ImResultPosition, 'IM result position').to.equal(0);
        expect(profileInfo.HimResultPosition, 'HIM result position').to.greaterThan(0);
        expect(profileInfo.HimResult, 'HIM result is null').not.to.be.null;
        expect(profileInfo.Name, 'Not equal names').to.equal(data.profileNameRus);
        expect(profileInfo.Country, 'Country xhould be RUS').to.equal('rus');
        expect(profileInfo.Sex, 'Gender should be Female').to.equal('F');
    })
    it('Test get user best results', async () => {
        const response = await api.Best(data.profileName).get();
        expect(response.status).to.equal(200);
        let profileBest = await response.json();
        expect(await profileBest.sprint.Racer, 'Best Sprint doesnt contain expected name').to.contains(data.profileSurnameEng);
        expect(await profileBest.olympic.Racer, 'Best Olympic doesnt contain expected name').to.equals(data.profileNameRus);
        expect(await profileBest.half.Racer.toLowerCase(), 'Best Half doesnt contain expected surname').to.contains(data.profileSurnameEng.toLowerCase());
    })
    it('Test get user results', async () => {
        const response = await api.ProfileResults(data.profileName).get();
        expect(response.status).to.equal(200);
        let profileResults = await response.json();
        expect(await profileResults.length, 'Profile results is empty').to.greaterThan(0);
    })
});
