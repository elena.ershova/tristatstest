import { urls } from '../frameworkUI/const/urls';
import { app } from '../frameworkUI/pages/index';
import log4js from 'log4js';

class Navigation {
    constructor(page) {
        this.page = page;
        this.logger = log4js.getLogger();
        this.logger.level = 'debug';
    }

    async gotoEvent(eventUrl) {
        let url = `${urls.eventUrlPrefix}${eventUrl}`;
        this.logger.info(`Go to url: ${url}`);
        await this.page.goto(url);
        return app.EventPage(this.page);
    }
}

export { Navigation };
