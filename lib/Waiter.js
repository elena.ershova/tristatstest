import log4js from 'log4js';
const logger = log4js.getLogger();
logger.level = 'debug';

const DEFAULT_INTERVAL = 100;
const DEFAULT_TIMEOUT = 500;

async function waitCondition(condition, message, timeout, interval) {
    logger.debug(message);
    if (interval === undefined) {
        interval = DEFAULT_INTERVAL;
    }
    if (timeout === undefined) {
        timeout = DEFAULT_TIMEOUT;
    }
    let time = 0;
    while (!condition || time < timeout) {
        await new Promise(resolve => setTimeout(resolve, interval));
        time += interval;
        if (condition) {
            break;
        }
        if (time > timeout) {
            throw new Error(`${message} | Waiting timeout is over: ${timeout}.`);
        }
    }
    logger.debug(`Waiting time is ${time}ms`)
    return condition;
}

async function wait(timeout) {
    logger.debug('Waiting...');
    if (timeout === undefined) {
        timeout = DEFAULT_TIMEOUT;
    }
    await new Promise(resolve => setTimeout(resolve, interval));
    logger.debug(`Waiting time is ${time}ms`);
}

export { waitCondition, wait };
